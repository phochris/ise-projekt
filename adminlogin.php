<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

	<title>Admin Backend</title>

	<!-- Bootstrap core CSS -->
	<link href="css/bootstrap.min.css" rel="stylesheet">

</head>

<body>

	<div class="container">
		<div class="row clearfix">
			<div class="col-md-12 column">
				<div class="row clearfix">
					<div class="col-md-4 column">
					</div>
					<div class="col-md-4 column" style="border-style:groove; padding-bottom:10px;">
						<?php
						if (isset($_GET["logout"]) && $_GET["logout"] == 1 ){
							?>
							<p class="alert alert-success">
								Erfolgreich ausgeloggt! <br>
							</p>

							<?php
						}
						?>

						<?php
						if (isset($_GET["loginfail"]) && $_GET["loginfail"] == 1 ){
							?>
							<p class="alert alert-danger">
								Der Login war nicht erfolgreich! <br>
								Bitte ueberpruefe Email/Passwort!
							</p>

							<?php
						}
						?>

						<?php
						if (isset($_GET["firstlogin"]) && $_GET["firstlogin"] == 1 ){
							?>
							<p class="alert alert-warning">
								Bitte logge dich zuerst ein! <br>
							</p>

							<?php
						}
						?>
						<h1 class="bg-success">Admin Login</h1>
						<form role="form" action="takelogin.php" method="post">
								<h1 class="cover-heading">Login</h1>
								<div class="form-group">
									<label for="inputemail">Email address</label>
									<input type="email" class="form-control" name="inputemail" placeholder="Enter email" required="required">
								</div>
								<div class="form-group">
									<label for="inputpassword">Password</label>
									<input type="password" class="form-control" name="inputpassword" placeholder="Password" required="required">
								</div>
								<input type="hidden" name="adminlogin" value="ok">
								<button type="submit" class="btn btn-success">Submit</button>
							</form>

					</div>
					<div class="col-md-4 column">
					</div>
				</div>
			</div>
		</div>
	</div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>
