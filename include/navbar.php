<?php include_once "/include/userquery.php"; ?> <!-- Userdaten werden eingebunden. -->
<nav class="navbar navbar-inverse" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="index.php">Webshop Austria</a>
	</div>

	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		<ul class="nav navbar-nav">

			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Hallo, <?php if(isset($_SESSION['Login'])) {echo $_SESSION['Name'];} else{echo "Gast";} ?> </span><strong class="caret"></strong></a>
				<ul class="dropdown-menu">
					<li >
						<a href="products.php">Produktliste</a>
					</li>
					<li >
						<?php
						if(isset($_SESSION["Login"]) && $_SESSION["Login"] = "ok")
						echo "<a href='konto.php'>Mein Konto</a>";
						else{echo "<a href='register.php?firstregister=1'>Mein Konto</a>";}
						?>
					</li>
					<li>
						<?php
						if(isset($_SESSION["Login"]) && $_SESSION["Login"] = "ok")
						echo "<a href='orders.php'>Meine Bestellungen</a>";
						else{echo "<a href='register.php?firstregister=1'>Meine Bestellungen</a>";}
						?>
					</li>
					<li>
						<a href="contact.php">Kontakt</a>
					</li>	
				</ul>
			</li>
		</ul>
		<form class="navbar-form navbar-left" role="search">
			<div class="form-group">
				<input type="text" class="search-query" />
			</div> <button type="submit" class="btn btn-sm">Suche</button>
		</form>
		<ul class="nav navbar-nav navbar-right">
			<li>
				<a href="kassa.php"><span class="glyphicon glyphicon-shopping-cart"></span> Warenkorb: <?php 

				if(isset($_SESSION['warenkorb'])){
					$summe = 0;
					$anzahl = count($_SESSION['warenkorb']);
					$korb=$_SESSION['warenkorb'];
					for ($count = 1 ; $count < $anzahl; $count++)
					{	
						$id= $korb[$count];
						$abfrage = "SELECT * FROM Produkt WHERE Produkt_ID LIKE '$id' LIMIT 1"; 
						$ergebnis = mysql_query($abfrage); 
						$row = mysql_fetch_object($ergebnis);    						
						$summe= $summe + $row->Preis;
					}
					echo $summe;
				}
				else {echo "0.00";}
				?> €</a>
			</li>
			<?php if(!isset($_SESSION['Login'])){ ?>
			<li>
				<a id="modal-435675" href="#modal-container-435675" role="button" class="btn" data-toggle="modal">Login</a>
			</li>

			<li>
				<a href="register.php">Registrieren</a>
			</li>
			<?php }
			else{ ?>
			<li>
				<a href="takelogout.php"> Logout</a>
			</li>
			<?php } ?>


			<div class="modal fade" id="modal-container-435675" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">× Close</button>
						</div>
						<div class="modal-body">
							<form role="form" action="takelogin.php" method="post">
								<h1 class="cover-heading">Login</h1>
								<div class="form-group">
									<label for="inputemail">Email address</label>
									<input type="email" class="form-control" name="inputemail" placeholder="Enter email" required="required">
								</div>
								<div class="form-group">
									<label for="inputpassword">Password</label>
									<input type="password" class="form-control" name="inputpassword" placeholder="Password" required="required">
								</div>
								<button type="submit" class="btn btn-success">Submit</button>
							</form>
						</div>
					</div>
					<div class="modal-footer">

					</div>
				</div>

			</div>
		</ul>
	</div>

</nav>
