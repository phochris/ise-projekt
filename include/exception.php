					<?php
						if (isset($_GET["registerFailed"]) && $_GET["reason"] == "emailused" ) {
					?>
						
						<p class="alert alert-danger">
							Email <b> <?php echo $_GET["e"]; ?> </b> wird bereits verwendet<br>
						</p>
					
					<?php
						}
					?>

					<?php
						if (isset($_GET["registerSucceed"]) && $_GET["registerSucceed"] == true && $_GET["register"] == "user") {
							if(isset($_GET["v"])){
					?>						
						<p class="alert alert-success">
							Benutzer <b> <?php echo $_GET["v"]." ".$_GET['n'];?> </b> wurde erstellt! Bitte überprüfen Sie Ihren Email-Account! <br>
						</p>					
					<?php
							}
							else{
					?>	
						<p class="alert alert-success">
							Firma <b> <?php echo $_GET["f"];?> </b> wurde erstellt! Bitte überprüfen Sie Ihren Email-Account! <br>
						</p>
					<?php
							}
						}
					?>

					<?php
						if (isset($_GET["registerFailed"]) && $_GET["reason"] == "password" ) {
					?>
					
						<p class="alert alert-danger">
							Passwörter stimmen nicht überein!
						</p>
					
					<?php
						}
					?>

					<?php //Admin Register
						if (isset($_GET["registerSucceed"]) && isset($_GET["register"]) && $_GET["register"] == "admin" ) {
							
					?>						
						<p class="alert alert-success">
							Admin <b> <?php echo $_GET["a"];?> </b> wurde erstellt!<br>
						</p>					
					<?php
							}
					?>	

					<?php //Product Register
						if (isset($_GET["addProduct"]) && isset($_GET["productName"])) {							
					?>						
						<p class="alert alert-success">
							Produkt <b> <?php echo $_GET["productName"];?> </b> wurde erstellt!<br>
						</p>					
					<?php
							}
					?>	

					<?php //Product create fail
						if (isset($_GET["addProduct"]) && $_GET["reason"] == "preis") {							
					?>						
						<p class="alert alert-danger">
							Bitte einen korrekten Preis angeben!<br>
						</p>					
					<?php
							}
					?>	

					<?php //Noch nicht registriert
						if (isset($_GET["firstregister"]) && $_GET["firstregister"] == 1) {							
					?>						
						<p class="alert alert-info">
							Bitte zuerst einloggen oder registrieren!<br>
						</p>					
					<?php
							}
					?>	

					<?php //Produkt in den Warenkorb gelegt
						if (isset($_GET["SucceedAdd"]) && $_GET["SucceedAdd"] == 1) {							
					?>						
						<p class="alert alert-success">
							Produkt '<?php echo $name ?>' wurde 1x in den Warenkorb gelegt.<br>
						</p>					
					<?php
							}
					?>	
					<?php //Produkt geändert
						if (isset($_GET["changeProduct"]) && $_GET["changeProduct"] == true) {							
					?>						
						<p class="alert alert-success">
							Produkt '<?php echo $name ?>' wurde geändert.<br>
						</p>					
					<?php
							}
					?>	