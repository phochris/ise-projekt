<?php
session_start();

if (isset($_SESSION["Login"]) && $_SESSION["Login"] == "ok") {
	session_destroy();
	$host= htmlspecialchars($_SERVER["HTTP_POST"]);
	$host= rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
	if(isset($_GET["admin"])){
		$extra="adminlogin.php?logout=1";
	}
	else{
		$extra="index.php?logout=1";
	}
	header("Location:$host$uri/$extra");
}
?>
