<?php
session_start();

if (isset($_SESSION["Login"]) && $_SESSION["Login"] == "ok" && isset($_SESSION["Admin_ID"])) {
	$verbindung = mysql_connect("a1200520mysql1.mysql.univie.ac.at", "a1200520" , "passwort123") or die ("Verbindung zur Datenbank konnte nicht hergestellt werden"); 

	mysql_select_db("a1200520mysql1") or die ("Datenbank konnte nicht ausgewählt werden"); 
	$admin_id=$_SESSION['Admin_ID'];

	$abfrage = "SELECT * FROM Admin WHERE Admin_ID LIKE '$admin_id' LIMIT 1"; 
	$ergebnis = mysql_query($abfrage); 
	$row = mysql_fetch_object($ergebnis); 
	?>

	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

		<title>Admin Backend</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

	</head>

	<body>

		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<nav class="navbar navbar-default navbar-inverse" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="adminindex.php">Webshop Austria</a>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li class="active">
									<a href="adminindex.php">Shop Übersicht</a>
								</li>
								<li>
									<a href="adminproducts.php">Produktliste</a>
								</li>
								<li>
									<a href="adminorders.php">Bestellungen</a>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="takelogout.php?admin=ok&logout=succeed"> Logout</a>
								</li>

							</ul>
						</div>

					</nav>
					<div class="row clearfix">
						<div class="col-md-4 column">
							
							<div class="panel panel-default">
								<div class="panel-heading">Infos über den Webshop:</div>
								<div class="panel-body">
									<ul class="list-unstyled">
										<li>
											<span class="glyphicon glyphicon-user"></span>
											<?php 
											$abfrage="SELECT * FROM Admin";
											$ergebnis=mysql_query($abfrage);
											$result=mysql_num_rows($ergebnis);
											echo "Anzahl der Admins: $result"; 
											?>

										</li>
										<li>
											<span class="glyphicon glyphicon-user"></span>
											<?php 
											$abfrage="SELECT * FROM Kunde";
											$ergebnis=mysql_query($abfrage);
											$result=mysql_num_rows($ergebnis);
											echo "Anzahl der Kunden(User): $result"; 
											?>

										</li>
										<li>
											<span class="glyphicon glyphicon-user"></span>
											<?php 
											$abfrage="SELECT * FROM Firma";
											$ergebnis=mysql_query($abfrage);
											$result=mysql_num_rows($ergebnis);
											echo "Anzahl der Kunden(Firma): $result"; 
											?>

										</li>
										<li>
											<span class="glyphicon glyphicon-user"></span>
											<?php 
											$abfrage="SELECT * FROM Kunde";
											$ergebnis=mysql_query($abfrage);
											$result1=(mysql_num_rows($ergebnis))+$result;
											echo "Anzahl der Kunden insgesamt: $result1"; 
											?>
										</li>
										<li>
											<span class="glyphicon glyphicon-ok"></span>
											<?php 
											$abfrage="SELECT * FROM Bestellung";
											$ergebnis=mysql_query($abfrage);
											$result=mysql_num_rows($ergebnis);
											echo "Anzahl der Bestellungen: $result"; 
											?>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="col-md-4 column">
							<div class="panel panel-default">
								<div class="panel-heading"><h3 class="panel-title">Neues Produkt erstellen:</h3></div>
								<div class="panel-body">
									<form class="form-horizontal" name="form1" role="form" action="addproduct.php" method="post" enctype="multipart/form-data">
										<?php
								include_once "include/exception.php"; // exception.php wird eingebunden.
								?>
								
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-exclamation-sign"></span> </span> 
									<input type="text" class="form-control" placeholder="Name" id="name" name="name" required="required">
								</div>
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-euro"></span></span>
									<input type="text" class="form-control" placeholder="Preis" id="preis" name="preis" required="required">
								</div>
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-pencil"></span></span>
									<textarea class="form-control" placeholder="Beschreibung" id="beschreibung" name="beschreibung" required="required" rows="5"></textarea>
								</div>
								<div class="input-group">
									<span class="input-group-addon">Kategorie</span>
									<select class="form-control" id="kategorie" name="kategorie" onchange="populate(this.id,'typ')">
										<option></option>
										<option value="Computer">Computer</option>
										<option value="Smartphone">Smartphone</option>
										<option value="Smartphone">Tablet</option>
										<option value="Software">Software</option>
										<option value="Drucker">Drucker</option>
									</select>
								</div>
								<div class="input-group">
									<span class="input-group-addon">Typ &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;</span>
									<select class="form-control" id="typ" name="typ" required="required">
									</select>
								</div>
								<div class="input-group">
									<span class="input-group-addon">Verfügbar</span>
									<select class="form-control" name="verfuegbarkeit">
										<option value="1">Ja</option>
										<option value="0">Nein</option>
									</select>
								</div>
								<div class="input-group">
									<span class="input-group-addon">Bild &#160;&#160;&#160;&#160;&#160;&#160;&#160;</span>
									<input type="file" name="upload">
								</div>
								<div class="form-group">
									<div class="col-sm-3">
										<button type="submit" class="btn btn-primary" onclick="allnumeric(document.form1.preis)" >Produkt erstellen</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-4 column">
					<div class="panel panel-default">
						<div class="panel-heading"> <h3 class="panel-title">Neuen Admin erstellen:</h3></div>
						<div class="panel-body">
							<form class="form-horizontal" role="form" action="registeradmin.php" method="post">
								<?php
								include_once "include/exception.php"; // exception.php wird eingebunden.
								?>
								
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span> 
									<input type="text" class="form-control" placeholder="Name" id="name" name="name" required="required">
								</div>
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
									<input type="password" class="form-control" placeholder="Passwort" id="passwort1" name="passwort1" required="required">
								</div>
								<div class="input-group">
									<span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
									<input type="password" class="form-control" placeholder="Passwort erneut eingeben" id="passwort2" name="passwort2" required="required">
								</div>
								<div class="input-group">
									<span class="input-group-addon">@</span>
									<input type="email" class="form-control" placeholder="Email" id="email1" name="email1" required="required"> 
								</div>
								<div class="input-group">
									<span class="input-group-addon">@</span>
									<input type="email" class="form-control" placeholder="Email erneut eingeben" id="email2" name="email2" required="required">
								</div>
								<div class="form-group">
									<div class="col-sm-3">
										<button type="submit" class="btn btn-primary">Admin erstellen</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="row clearfix">
				<div class="col-md-4 column">
				</div>
				<div class="col-md-4 column">
				</div>
				<div class="col-md-4 column">
				</div>
			</div>
		</div>
	</div>
</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    <script type="text/javascript">
    function populate(s1,s2){
    	var s1 = document.getElementById(s1);
    	var s2 = document.getElementById(s2);
    	s2.innerHTML = "";
    	if(s1.value == "Computer"){
    		var optionArray = ["|","apple|Apple","samsung|Samsung","asus|Asus", "hp|HP"];
    	} else if(s1.value == "Smartphone"){
    		var optionArray = ["|","apple|Apple","samsung|Samsung"];
    	} else if(s1.value == "Tablet"){
    		var optionArray = ["|","ipad|Ipad"];
    	} else if(s1.value == "Software"){
    		var optionArray = ["|","photoshop|Photoshop","adobe|Adobe" , "windows|Windows"];
    	} else if(s1.value == "Drucker"){
    		var optionArray = ["|","hp|HP","samsung|Samsung"];
    	}
    	for(var option in optionArray){
    		var pair = optionArray[option].split("|");
    		var newOption = document.createElement("option");
    		newOption.value = pair[0];
    		newOption.innerHTML = pair[1];
    		s2.options.add(newOption);
    	}
    }

    function allnumeric(inputtxt)  
    {  
    	var numbers = /^[0-9]+$/;  
    	if(!inputtxt.value.match(numbers))  
    	{  
    		alert('Please input numeric characters only');  
    		document.form1.text1.focus();  
    		window.location.reload();
    		return false;  
    	}  
    }   
    </script>
</body>
</html>

<?php
} else {
	$host= htmlspecialchars($_SERVER["HTTP_POST"]);
	$host= rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
	$extra="adminlogin.php?firstlogin=1";
	header("Location:$host$uri/$extra");
}
?>
