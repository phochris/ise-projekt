<?php
session_start();

if (isset($_SESSION["Login"]) && $_SESSION["Login"] == "ok" && isset($_SESSION["Admin_ID"])) {
	$verbindung = mysql_connect("a1200520mysql1.mysql.univie.ac.at", "a1200520" , "passwort123") or die ("Verbindung zur Datenbank konnte nicht hergestellt werden"); 

	mysql_select_db("a1200520mysql1") or die ("Datenbank konnte nicht ausgewählt werden"); 
	$admin_id=$_SESSION['Admin_ID'];

	$abfrage = "SELECT * FROM Admin WHERE Admin_ID LIKE '$admin_id' LIMIT 1"; 
	$ergebnis = mysql_query($abfrage); 
	$row = mysql_fetch_object($ergebnis); 
	?>

	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

		<title>Admin Backend</title>

		<!-- Bootstrap core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">

	</head>

	<body>

		<div class="container">
			<div class="row clearfix">
				<div class="col-md-12 column">
					<nav class="navbar navbar-default navbar-inverse" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button> <a class="navbar-brand" href="adminindex.php">Webshop Austria</a>
						</div>

						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
							<ul class="nav navbar-nav">
								<li>
									<a href="adminindex.php">Shop Übersicht</a>
								</li>
								<li class="active">
									<a href="adminproducts.php">Produktliste</a>
								</li>
								<li>
									<a href="adminorders.php">Bestellungen</a>
								</li>
							</ul>
							<ul class="nav navbar-nav navbar-right">
								<li>
									<a href="takelogout.php?admin=ok&logout=succeed"> Logout</a>
								</li>

							</ul>
						</div>

					</nav>
					<div class="row clearfix">
						<div class="col-md-8 column">
							<form id='searchform' action='adminproducts.php' method='get'>
								<a href='adminproducts.php'>Alle Produkte</a> ---
								Suche nach Produkt Titel:
								<input id='search' name='search' type='text' size='20' value='<?php if(isset($_GET['search'])) echo $_GET['search']; ?>' />
								<input id='submit' type='submit' value='Los!' />
							</form>
							<?php

							$anfang = 0;

							if (isset($_GET["anfang"]))
								$anfang = urldecode($_GET["anfang"]);


							$sql2 = "SELECT Count(*) FROM Produkt";
							$AnzahlProdukte = mysql_query($sql2);

							$row = mysql_fetch_row($AnzahlProdukte);
							echo " <h3>Das Sortiment hat $row[0] Produkte:</h3>";
							$anfangNext = $anfang+10;
							if( $anfangNext> $row[0])
							{
								$anfangNext=$row[0] - $row[0]%10;
							}

							$anfangBefore = $anfang - 10;
							if( $anfangBefore> $row[0])
							{
								$anfangBefore=0;
							}

							if($anfang==0){
								if (isset($_GET['search'])) {
									$sql = "SELECT * FROM Produkt WHERE Name like '%" . $_GET['search'] . "%'";
								} 
								else {
									$sql = "SELECT * FROM Produkt Limit 0, 10";
								}
								
							}else{
								if (isset($_GET['search'])) {
									$sql = "SELECT * FROM Produkt WHERE Name like '%" . $_GET['search'] . "%'";
								} 
								else {
									$sql = "SELECT * FROM Produkt Limit ". $anfang. " , 10";
								}

								
							}
							$stmt = mysql_query($sql);

							if(!$stmt){
								die('Could not connect: ' . mysql_error());
								echo "Hallo".mysql_error();	
							}

							?>
							<div class="panel panel-default">
								<div class="panel-body">

									<table class="table table-condensed table-hover">
										<thead>
											<tr>
												<th>Name</th>
												<th width="300px">Beschreibung</th>
												<th width="100px">Preis <span class='glyphicon glyphicon-euro'></span> </th>
												<th>Kategorie</th>
												<th>Marke</th>
												<th>Gekauft</th>
												<th>Verfügbarkeit</th>
												<th width="150px"></th>
											</tr>
										</thead>
										<tbody>
											<?php
  											// fetch rows of the executed sql query  
											while ($row = mysql_fetch_array($stmt)) {
												$id=$row['Produkt_ID'];
												echo "<tr>";
												?>
												<td> <a href='admindetail.php?id=<?php echo $id?>'> <?php echo $row['Name'] ?>  </a>  </td>
												<?php
												echo "<td>" . $row['Beschreibung'] . "</td>";
												echo "<td>" . $row['Preis'] ."</td>";
												echo "<td>" . $row['Kategorie'] . "</td>";
												echo "<td>" . $row['Typ'] . "</td>";
												echo "<td> 0x </td>";
												if($row['Verfuegbarkeit'] == 1)
													echo "<td style='color:green;'> Ja</td>";
												else {
													echo "<td style='color:red;'> Nein</td>";
												}
												echo "<td> <a href='admindetail.php?id=$id'><span class='glyphicon glyphicon-pencil'></span></a> </td>";

												echo "</tr>";
											}
											?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="col-md-4 column">

						</div>
						<div class="row clearfix">
							<div class="col-md-4 column">
							</div>
							<div class="col-md-4 column">
							</div>
							<div class="col-md-4 column">
							</div>
						</div>
					</div>
				</div>
			</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="../../dist/js/bootstrap.min.js"></script>
    
</body>
</html>

<?php
} else {
	$host= htmlspecialchars($_SERVER["HTTP_POST"]);
	$host= rtrim(dirname(htmlspecialchars($_SERVER["PHP_SELF"])), "/\\");
	$extra="adminlogin.php?firstlogin=1";
	header("Location:$host$uri/$extra");
}
?>
