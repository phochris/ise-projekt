<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Webshop Austria</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  </head>
  <body>
  	<div class="container">
	<div class="row clearfix">
		<div class="col-md-12 column">
			<?php include_once "/include/navbar.php"; ?> <!-- Navbar wird eingebunden. -->
			<div class="row clearfix">
				<div class="col-md-4 column">
					<?php
					include_once "include/exception.php"; // exception.php wird eingebunden.
					?>
					<form role="form" action="takelogin.php" method="post">
								<h3 class="cover-heading">Login</h3>
								<div class="form-group">
									<label for="inputemail">Email address</label>
									<input type="email" class="form-control" name="inputemail" placeholder="Enter email" required="required">
								</div>
								<div class="form-group">
									<label for="inputpassword">Password</label>
									<input type="password" class="form-control" name="inputpassword" placeholder="Password" required="required">
								</div>
								<button type="submit" class="btn btn-primary">Submit</button>
							</form>
				</div>
				<div class="col-md-8 column">
					<div class="tabbable" id="tabs-293271">
					<h3 class="cover-heading">Registrieren</h3>	
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#panel-755938" data-toggle="tab">Privatperson</a>
							</li>
							<li>
								<a href="#panel-294237" data-toggle="tab">Firma</a>
							</li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="panel-755938">
								<br/>
								<form class="form-horizontal" role="form" action="registeruser.php" method="post">
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span> 
									  	<input type="text" class="form-control" placeholder="Vorname" id="vorname" name="vorname" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span> </span> 
									  <input type="text" class="form-control" placeholder="Nachname" id="nachname" name="nachname" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
									  <input type="password" class="form-control" placeholder="Passwort" id="passwort1" name="passwort1" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
									  <input type="password" class="form-control" placeholder="Passwort erneut eingeben" id="passwort2" name="passwort2" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon">@</span>
									  <input type="email" class="form-control" placeholder="Email" id="email1" name="email1" required="required"> 
									</div>
									<div class="input-group">
									  <span class="input-group-addon">@</span>
									  <input type="email" class="form-control" placeholder="Email erneut eingeben" id="email2" name="email2" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Postleitzahl" id="plz" name="plz" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Ort" id="ort" name="ort" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Straße" id="strasse" name="strasse" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Hausnummer" id="hausnr" name="hausnr" required="required">
									</div>
									<div class="form-group">
						              <div class="col-sm-3">
						                 <button type="submit" class="btn btn-primary">Registrieren</button>
						              </div>
						            </div>
								</form>
							</div>
							<div class="tab-pane" id="panel-294237">
								<br/>
								<form class="form-horizontal" role="form" action="registercompany.php" method="post">
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-home"></span> </span> 
									  	<input type="text" class="form-control" placeholder="Firmenname" id="fname" name="fname" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
									  <input type="password" class="form-control" placeholder="Passwort" id="passwort1" name="passwort1" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-eye-open"></span></span>
									  <input type="password" class="form-control" placeholder="Passwort erneut eingeben" id="passwort2" name="passwort2" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon">@</span>
									  <input type="email" class="form-control" placeholder="Email" id="email1" name="email1" required="required"> 
									</div>
									<div class="input-group">
									  <span class="input-group-addon">@</span>
									  <input type="email" class="form-control" placeholder="Email erneut eingeben" id="email2" name="email2" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Postleitzahl" id="plz" name="plz" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Ort" id="ort" name="ort" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Straße" id="strasse" name="strasse" required="required">
									</div>
									<div class="input-group">
									  <span class="input-group-addon"><span class="glyphicon glyphicon-road"></span></span>
									  <input type="text" class="form-control" placeholder="Hausnummer" id="hausnr" name="hausnr" required="required">
									</div>
									<div class="form-group">
						              <div class="col-sm-3">
						                 <button type="submit" class="btn btn-primary">Registrieren</button>
						              </div>
						            </div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>
	
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>

 </body>
</html>